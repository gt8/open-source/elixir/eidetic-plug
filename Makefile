TESTS=

.PHONY: deps test

up:
	@mix run --no-halt

hex:
	@mix do local.hex --force, local.rebar --force

deps: hex
	@mix deps.get

compile: hex
	@mix compile

test:
	@mix test ${TESTS}

lint: hex
	@mix credo --strict

iex: hex
	@iex -S mix

clean: hex
	@mix clean --deps

dshell:
	@docker-compose run --rm elixir bash
