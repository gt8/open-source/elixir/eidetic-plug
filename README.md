# Eidetic Plug

**Note:** The canonical repository is hosted [here](https://gitlab.com/gt8/open-source/elixir/eidetic-plug), on GitLab.com.

[![Hex.pm](https://img.shields.io/hexpm/v/eidetic_plug.svg)](https://hex.pm/packages/eidetic_plug)
[![Hex.pm](https://img.shields.io/hexpm/l/eidetic_plug.svg)](https://hex.pm/packages/eidetic_plug)
[![Hex.pm](https://img.shields.io/hexpm/dw/eidetic_plug.svg)](https://hex.pm/packages/eidetic_plug)
[![build status](https://gitlab.com/gt8/open-source/elixir/eidetic_plug/badges/master/pipeline.svg)](https://gitlab.com/gt8/open-source/elixir/eidetic-plug/commits/master)
[![code coverage](https://gitlab.com/gt8/open-source/elixir/eidetic-plug/badges/master/coverage.svg)](https://gitlab.com/gt8/open-source/elixir/eidetic-plug/commits/master)

Plug that allows setting Eidetic metadata from the HTTP request

## Installation

```elixir
def deps do
  [
    {:eidetic_plug, "~> 0.3.0"}
  ]
end
```

## Usage

```elixir
plug Eidetic.Plug, [:remote_ip]
```

For further information, please check the [https://hex.pm/eidetic-plug](documentation).
