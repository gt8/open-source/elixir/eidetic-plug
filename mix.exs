defmodule EideticPlug.Mixfile do
  use Mix.Project

  def project do
    [ app: :eidetic_plug,
      version: "0.3.0",
      elixir: "~> 1.3",
      start_permanent: Mix.env == :prod,
      deps: deps(),
      description: description(),
      package: package(),
      test_coverage: [
        tool: ExCoveralls
      ]
    ]
  end

  defp description() do
    """
    Plug that allows setting Eidetic metadata from the HTTP request
    """
  end

  defp package do
    [ name: :eidetic_plug,
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["GT8Online"],
      licenses: ["MIT"],
      links: %{"Source Code" => "https://gitlab.com/gt8/open-source/elixir/eidetic-plug"}]
  end

  def application do
    [ extra_applications: [:logger] ]
  end

  defp deps do
    [ {:plug,         "~> 1.4"},

      {:credo,        ">= 0.0.0",   [only: [:dev, :test]]},
      {:ex_doc,       ">= 0.0.0",   [only: [:dev, :test]]},
      {:excoveralls,  "~> 0.6",     [only: [:dev, :test]]},
      {:faker,        "~> 0.9.0",   [only: [:dev, :test]]},
    ]
  end
end
