defmodule Eidetic.Plug do
  @moduledoc """
  Plug that allows setting Eidetic metadata from the HTTP request

  ```elixir
  # Plug accepts a Keyword List of items to fetch from the request and add to
  #   the Eidetic metadata on any events persisted for this request
  plug Eidetic.Plug, [:remote_ip]
  ```
  """
  defimpl Poison.Encoder, for: Tuple do
    def encode(tuple, options) do
      tuple
      |> Tuple.to_list()
      |> Poison.encode!()
    end
  end

  def init(options), do: options

  def call(conn, options) do
    handle_metadata_request(conn, options)
  end

  defp handle_metadata_request(conn, [:remote_ip | options]) do
    conn
    |> Map.get(:remote_ip)
    |> Tuple.to_list()
    |> Enum.join(".")
    |> store(:remote_ip)

    handle_metadata_request(conn, options)
  end

  defp handle_metadata_request(conn, [option | options]) do
    conn
    |> Map.get(option)
    |> store(option)

    handle_metadata_request(conn, options)
  end

  defp handle_metadata_request(conn, []) do
    conn
  end

  defp store(nil, _option) do
  end

  defp store(value, option) when is_binary(option) do
    eidetic_map = :eidetic
    |> Process.get(%{})
    |> Map.merge(%{option => value})

    Process.put(:eidetic, eidetic_map)
  end

  defp store(value, option) when is_atom(option) do
    eidetic_map = :eidetic
    |> Process.get(%{})
    |> Map.merge(Map.put(%{}, option, value))

    Process.put(:eidetic, eidetic_map)
  end
end
