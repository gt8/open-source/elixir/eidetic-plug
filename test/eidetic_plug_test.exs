defmodule Eidetic.PlugTest do
  use ExUnit.Case, async: true
  use Plug.Test

  test ~s/no options/ do
    conn = conn(:get, "/", "")
    |> Eidetic.Plug.call([])

    eidetic_map = Process.get(:eidetic, %{})

    assert conn == conn
    assert %{} == eidetic_map
  end

  test ~s/it gets the request ip address/ do
    conn = conn(:get, "/", "")
    |> Eidetic.Plug.call([:remote_ip])

    eidetic_map = Process.get(:eidetic, %{})

    assert conn == conn
    assert %{remote_ip: "127.0.0.1"} = eidetic_map
  end
end
